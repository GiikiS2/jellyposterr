export interface jellyfin {
    url: string,
    username: string,
    api_Key: string
}

interface output {
    raw: any
    watching: boolean
}

export async function get(config: jellyfin): Promise<any> {
    config.url = config.url.replace(/\/$/, '');
    const response = await fetch(`${config.url}/Sessions?api_key=${config.api_Key}`);
    const sessions: any = await response.json();

    for (const session of sessions) {

        if (!session.UserName) {
            return;
        }

        const sessionUsername = session.UserName.toLowerCase();

        if (
            (config.username && sessionUsername !== (config.username as string).toLowerCase())
        ) {
            continue;
        }

        if (!session.NowPlayingItem) {

            var items = await fetch(`${config.url}/Items/Suggestions?type=Audio&type=AudioBook&type=Book&type=Channel&type=Movie&type=LiveTvChannel&type=LiveTvProgram&type=MusicAlbum&type=MusicArtist&type=MusicGenre&type=MusicVideo&type=Season&type=Series&limit=1&enableTotalRecordCount=false&api_key=${config.api_Key}`);
            var witems = await items.json();

            var outraw = witems.Items[0]

            if (outraw.Type == 'Season') {
                var seasontitle = `${outraw.SeriesName} - ${outraw.Name}`
                outraw.SeriesName = seasontitle
            } else if (outraw.Type == 'Movie' || 'Series') {
                outraw.SeriesName = outraw.Name
                outraw.SeriesId = outraw.Id
            }

            var out: output = {
                raw: witems.Items[0],
                watching: false
            }
            return out
        }

        var out: output = {
            raw: session.NowPlayingItem,
            watching: true
        }
        return out
    }
}
